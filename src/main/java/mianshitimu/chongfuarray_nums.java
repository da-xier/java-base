package mianshitimu;

import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName chongfuarray_nums
 * @Author lupeibei
 * @create 2022/6/21 10:56 PM
 */
public class chongfuarray_nums {
    //数组中[2, 3, 1, 0, 2, 5, 3]输出重复的数字中的任一数字
    public void nums(int [] arr) {
        Set<Integer> set = new HashSet<Integer>();
        int repeat = -1;
        for (int num : arr) {
            if (!set.add(num)) {
                repeat = num;
                break;
            }
        }
        System.out.println(repeat);
    }
    public static void main(String[] args) {
        chongfuarray_nums chongfuarray_nums =new chongfuarray_nums();
        int [] arr={2, 3, 1, 0, 2, 5, 3};
        chongfuarray_nums.nums(arr);
    }
}
