package mianshitimu;

import java.util.Arrays;

/**
 * @ClassName arr_samenum
 * @Author lupeibei
 * @create 2022/6/22 12:59 PM
 */
public class arr_samenum {
    //nums1 = [1,2,2,1], nums2 = [2,2]找出相同的数字
    public  void find_same_num(int [] num1,int [] num2) {
        Arrays.sort(num1);
        Arrays.sort(num2);
        int i = 0, j = 0;
        int res = -1;
        while (i < num1.length - 1 && j < num2.length - 1) {
            if (num1[i] < num2[j]) i++;
            else if (num1[i] > num2[j]) j++;
            else {
                res = num1[i];
                break;
            }
        }
        System.out.println(res);
    }
    public static void main(String[] args) {
        arr_samenum arr_samenum=new arr_samenum();
        int [] num1={1,3,5,6,7};
        int [] num2={6,7};
        arr_samenum.find_same_num(num1,num2);


    }
}
