package mianshitimu;

/**
 * @ClassName array_sort
 * @Author lupeibei
 * @create 2022/6/21 8:35 PM
 */
public class array_sort {
    //[1, 7, 8, 4, 3]  最后数组展示为 [1, 3, 4, 7, 8]
    public void bubblesort(int[] arr) {
        //冒泡排序
        if (arr == null || arr.length < 2) {
            return;
        }
        int N = arr.length;
        for (int end = N - 1; end >= 0; end--) {
            for (int second = 1; second <= end; second++) {
                if (arr[second - 1] > arr[second]) {
                    swp(arr, second - 1, second);
                }
            }
        }
    }
    public  void insertsort(int [] arr) {
        //插入排序
        if(arr==null||arr.length<2){
            return;
        }
        int n=arr.length;
        for (int end = 1; end  < n; end ++) {
            int newNumIndex =end;
            if (newNumIndex-1>=0 && arr[newNumIndex-1]>arr[newNumIndex]){
                swp(arr,newNumIndex-1,newNumIndex);
                newNumIndex--;
            }
        }
    }




    public void swp(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public void printarray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                System.out.print("[");
            }
            if (i == arr.length - 1) {
                System.out.println(arr[i] + "]");
            } else {
                System.out.print(arr[i] + ",");
            }
        }
//        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {1, 7, 8, 4, 3};
        //为了不那么调用静态方法，
        array_sort test = new array_sort();
        test.bubblesort(arr);
        test.printarray(arr);
        test.insertsort(arr);
    }
}
