package mianshitimu;

import leetcode.max_consecutive_ones;

/**
 * @ClassName maxnum_cishu
 * @Author lupeibei
 * @create 2022/6/21 8:46 PM
 */
public class maxnum_cishu {
    //[1,1,0,1,1,1]中一个数连续出现的最高的频率
    public void test(int [] arr){
        int maxcount=0,count=0;
        for (int i = 0; i <arr.length ; i++) {
            if (arr[i]==1){
                count++;
            }else {
                maxcount=Math.max(maxcount, count);
                count=0;
            }
        }
        maxcount = Math.max(maxcount, count);
        System.out.println(maxcount);
    }

    public static void main(String[] args) {
        max_consecutive_ones maxsum=new max_consecutive_ones();
        int [] arr={1,1,0,1,1,1};
        maxsum.test(arr);
    }
}
