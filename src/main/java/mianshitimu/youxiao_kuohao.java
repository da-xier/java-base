package mianshitimu;

import java.util.HashMap;
import java.util.Stack;

/**
 * @ClassName youxiao_kuohao
 * @Author lupeibei
 * @create 2022/6/22 1:24 AM
 */
public class youxiao_kuohao {
    public void par(String s) {
        HashMap<Character, Character> map = new HashMap<>();
        map.put(')', '(');
        map.put('}', '{');
        map.put(']', '[');
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            //如果是左括号，直接进栈
            if (!map.containsKey(s.charAt(i))) {
                stack.push(s.charAt(i));
            } else {
                //如果是右括号，出栈然后判断出栈左括号对应HashMap的左括号是否一致
                char top = stack.isEmpty() ? '#' : stack.pop();
                if (top != map.get(s.charAt(i))) {
                    System.out.println(false);
                    return;
                }
            }
        }
        System.out.println(true);
    }

    public static void main(String[] args) {
        youxiao_kuohao youxiao_kuohao=new youxiao_kuohao();
        String s="()[]{}";
        youxiao_kuohao.par(s);
    }
    }
