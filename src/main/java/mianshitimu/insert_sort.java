package mianshitimu;

import java.util.Arrays;

/**
 * @ClassName insert_sort
 * @Author lupeibei
 * @create 2022/6/22 8:42 AM
 */
public class insert_sort {
    //插入排序

    public  void sort(int[] num) {
        //从第一个元素开始认为已经排序，取出下一个元素，如果
        int j;
        for (int i = 1; i < num.length; i++) {
            //记录要插入的值，这时就有了一个长度为i+1的数组可以进行移动
            int temp = num[i];
            for (j = i - 1; j >= 0 && num[j] > temp; j--) {
                num[j + 1] = num[j];
            }
            num[j + 1] = temp;
        }
        System.out.println(Arrays.toString(num));
    }
    public static void main(String[] args) {
        insert_sort sort=new insert_sort();
        int[] num = {3,44,38,5,47,15,36,26,27,2,46,4,19,50,48};
        sort.sort(num);
    }

    }

