package mianshitimu;

import java.util.Arrays;

/**
 * @ClassName twoarray_onearray
 * @Author lupeibei
 * @create 2022/6/21 8:38 PM
 */
public class twoarray_onearray {
    //现有2个有序的数组[1,4,6,8],[2,3,7],需要把他变成有序的数组[1,2,3,4,6,7,8]
    //
    public void test02() {
        int[] arr0 = { 1,4,6,8};
        int[] arr1 = { 2,3,7 };
        int[] arr2 = new int[arr0.length + arr1.length];
        for (int i = 0; i < arr0.length; i++) {
            int num = arr0[i];
            arr2[i] = num;
        }
        for (int i = 0; i < arr1.length; i++) {
            int num = arr1[i];
            arr2[arr0.length + i] = num;
        }
        Arrays.sort(arr2);
        System.out.println(Arrays.toString(arr2));
    }

    public static void main(String[] args) {
        twoarray_onearray twoarray_onearray=new twoarray_onearray();
//        twoarray_onearray.sort();
        twoarray_onearray.test02();
    }


}
