package mianshitimu;

import java.util.HashSet;

/**
 * @ClassName onlychar
 * @Author lupeibei
 * @create 2022/6/22 12:08 AM
 */
public class onlychar {
    //判断s = "leetcode"字符中的是否都相同
    //新建一个hashset的集合，将s中的数字依次放入到集合中，如果放不进去，就跑出异常
    public void onlychar_sum(String astr) {
        HashSet arraye = new HashSet();
        for (int i = 0; i < astr.length(); i++) {
            if (!arraye.add(astr.charAt(i))) {
                System.out.println(false);
                return;
            }
        }
        System.out.println(true);
    }
    public static void main(String[] args) {
        onlychar onlychar =new onlychar();
        String astr="leetcode";
        onlychar.onlychar_sum(astr);
    }
}
