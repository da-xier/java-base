package others;

/**
 * @ClassName RandToRand
 * @Author lupeibei
 * @create 2022/5/26 12:21 AM
 */
public class RandToRand {
    public static void main(String[] args) {
        System.out.println("测试开始");
        int testTimes =10000000;
        int count =0;
        for (int i = 0; i < testTimes; i++) {
            if (Math.random()<0.3){
                count++;
            }
        }
        System.out.println((double) count/(double) testTimes);
        System.out.println("=========");
        count=0;
        double ans =Math.random()*8;
        for (int i = 0; i < testTimes; i++) {
            if (Math.random()*8 < 4) {
                count++;
            }
            }
        System.out.println((double) count/(double) testTimes);
    }
}
