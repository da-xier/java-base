package leetcode;

import java.util.HashMap;
import java.util.Stack;

/**
 * @ClassName valid_parentheses
 * @Author lupeibei
 * @create 2022/6/5 11:18 PM
 */
public class valid_parentheses{
    public void valid(String s) {
        int n = s.length();
        if (n < 2 || n % 2 != 0) {
            return;
        }
        HashMap<Character, Character> sites = new HashMap<>();
        sites.put(')', '(');
        sites.put('}', '{');
        sites.put(']', '[');
        Stack<Character> shu = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            if (sites.containsKey(s.charAt(i))) {
                shu.push(s.charAt(i));
            } else {
                char top = shu.isEmpty() ? '#' : shu.pop();
                if (top != sites.get(s.charAt(i))) {
                    System.out.println(false);
                }
            }
        }



    }

    public static void main(String[] args) {
        valid_parentheses valid_parentheses=new valid_parentheses();
        String s="()[]{}";
        valid_parentheses.valid(s);

    }

}
