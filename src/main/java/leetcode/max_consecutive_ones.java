package leetcode;

/**
 * @ClassName max_consecutive_ones
 * @Author lupeibei
 * @create 2022/6/5 10:13 PM
 */
public class max_consecutive_ones {
    public void test(int [] arr){
        int maxcount=0,count=0;
        for (int i = 0; i <arr.length ; i++) {
            if (arr[i]==1){
                count++;
            }else {
                maxcount=Math.max(maxcount, count);
                count=0;
            }
        }
        maxcount = Math.max(maxcount, count);
        System.out.println(maxcount);

    }

    public static void main(String[] args) {
        max_consecutive_ones maxsum=new max_consecutive_ones();
        int [] arr={1,1,0,1,1,1};
        maxsum.test(arr);
    }
}
