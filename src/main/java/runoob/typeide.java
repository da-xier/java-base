package runoob;

/**
 * @ClassName typeide
 * @Author lupeibei
 * @create 2022/6/5 6:23 PM
 */
public class typeide {
////    //局部变量,age是一个局部变量若局部变量没有被初始化，那么编译会报错
////    public void  PUPAge(){
////        int age =0;
////        age=age+7;
////        System.out.println("小狗的年纪是："+age);
////    }
////    public static void main(String[] args) {
////        typeide typeide =new typeide();
////        typeide.PUPAge();
//
//    }
    //实例变量,实例变量在一个类中，但方法和构造方法和语句块之外
    //
    public String name;
    private  double salary;
    public typeide(String empName){
        name=empName;
    }
    public void  setSalary(double empSal){
        salary=empSal;
    }
    public void  printEmp(){
        System.out.println("名字："+name);
        System.out.println("薪水："+salary);
    }

    public static void main(String[] args) {
        typeide typeide =new typeide("RUNOOB");
        typeide.setSalary(1000.0);
        typeide.printEmp();

    }
}
