package other;

public class queryarray {
    //查询指定数组的元素的位置，如果不存在就返回
    public static void main(String[] args){
        int [] arr={2,45,34,12,16};
        int index=-1;
        for (int i=0;i<=arr.length;i++){
            if (arr[i]==45){
                index=i;
                break;
            }
        }
        if(index!=-1){
            System.out.println("元素对应的索引"+index);
        }else {
        System.out.println("查无此数");
        }
    }
    //定义一个方法，查询数组中指定元素对应的索引
    //不确定因素：哪个数组，哪个指定元素
    //返回值：索引

}
