package other;

import java.util.Scanner;

public class StuScore {
    //录制10个学生成绩，并将学生成绩求和，求平均数，查询某个学生特定的分数
    public static void main(String[] args) {
        //插入数组
        int scores[] = new int[10];
        //求和
        int sum = 0;
        //求平均数
        //输入成绩
        for (int i = 1; i <= 10; i++) {
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入第" + i + "个学生成绩");
            int score = sc.nextInt();
            scores[i - 1] = score;
            sum = sum + score;
        }
        System.out.println("所有同学的成绩" + sum);
        System.out.println(scores[6]);
        //正向遍历
        for (int j=0;j<=9;j++){
            System.out.println("第"+(j+1)+"个同学的成绩"+scores[j]);
        }
        //增强for循环
        //对scores数组进行遍历，遍历出来每个元素都用int类型的num接收
        int count=0;
        for(int num:scores){
            count++;
            System.out.println(num);
        }
    }
         //逆向遍历


}
