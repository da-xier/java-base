package other;

public class maxarray {
    //给定一个数组int[] arr={12,3,4,5,6,12,13,45},求数组中最大的数
    //简单写法
//    public static void main(String[] args){
//        int arr[]={12,3,4,5,6,12,13,45};
//        int maxnum =arr[0];
//        for(int i=0;i<arr.length;i++){
//            if(arr[i]>maxnum){
//                maxnum=arr[i];
//            }
//        }
//        System.out.println("当前数组中最大的数为"+maxnum);
//    }
    //想提取一个方法：求数组中的最大值
    public static void main(String[] args){
        int arr[]={12,3,4,5,6,12,13,45};
        int num=getMaxNum(arr);
        System.out.println("当前数组最大值为："+num);

    }
    public static int getMaxNum(int[] arr){
        int maxnum=arr[0];
        for(int i=0;i<arr.length;i++){
            if (arr[i]>maxnum){
                maxnum=arr[i];
            }
        }
        return maxnum;
    }
}

