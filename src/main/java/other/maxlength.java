package other;

public class maxlength {
    //    public static void main(String[] args){
//        String[] list={"flower","flow","flight"};
//        //遍历数组并输出
//        int n=list.length;
//        for(int i=0;i<n;i++){
//            System.out.println(list[i]);
//            if((i+1)>(n-1)){
//                return;
//            }
//            for(int j=0;j<n;j++){
//                if(list[i].charAt(j)==list[i+1].charAt(j+1)){
//                    System.out.println(list[0].charAt(0));
//                }else {
//                    return;
//                }
//            }
//
//        }
//        //取数组中的第一个字符（想项）
////        System.out.println(list[0].charAt(0));
////        System.out.println(list[1].charAt(0));
//    }
    public String longestCommonPrefix(String[] strs) {
        //strs数组的长度为0，则直接返回空
        if (strs.length == 0)
            return "";
        //新定义一个ans的数组和strs一样
        String ans = strs[0];
        //加一个循环，i数值是ans数组的长度，
        for (int i = 1; i < strs.length; i++) {
            //
            int j = 0;
            for (; j < ans.length() && j < strs[i].length(); j++) {
                if (ans.charAt(j) != strs[i].charAt(j))
                    break;
            }
            ans = ans.substring(0, j);
            if (ans.equals(""))
                return ans;
        }
        return ans;
    }

}
