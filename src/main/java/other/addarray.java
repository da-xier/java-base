package other;

import java.util.Arrays;
import java.util.Scanner;

public class addarray {
    //给定一个数组，在数组下标为2的位置上添加一个元素为91的数
    public static void main(String[] args) {
        //数组写死
//        int array[]={2,34,25,35,67,34,778};
        //数组随机输入
        int numlist[] = new int[10];
        for (int i = 0; i <= 9; i++) {
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入第" + (i+1) + "个数字来拼成完整的数组");
            int num = sc.nextInt();
            numlist[i] = num;
        }
        System.out.println("增加数字前的数组"+Arrays.toString(numlist));
        //替换当前元素，变成修改后的数组
        int index=2;
        int value=5;
        numlist[index]=value;
        System.out.println("换数字后的数组"+Arrays.toString(numlist));
        //2.增加当前的数组
        int newNumList []=insert(numlist,2,6666);
        System.out.println("新增最终结果"+Arrays.toString(newNumList));
        //

        //输出增加前的元素

    }
    // 增加数组的方法：i表示数组索引，int表示插入的数   data表示原数组
    private static int[] insert(int[] numlist, int i, int key ) {
        //数组扩容，第一步：定义一个更大的数组
        int[] data2=new int[numlist.length+1];
        //第二步：把数组中【0，i)范围的元素复制到data2对应的位置
        for (int j = 0; j < i; j++) {
            data2[j]= numlist[j];
        }
        //第三步:把key元素保存到 data2[i]的位置
        data2[i]=key;
        //第四步：把数组data中(i,data.length)范围的元素复制到data2数组从 i+1对应的位置。
        for (int k = i; k <numlist.length ; k++) {
            data2[k+1]=numlist[k];
        }
        return data2;

    }

}
