package other;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class stringcase {
    public static void main(String[] args) {
        String test = " Java easy method ";
        String abc ="JJJ";
        int [] test1={1,2,3,4,5,6,7,8,9,10,11,12};
        System.out.println(test.charAt(5));//输入字符串的第几位
        System.out.println(test.indexOf("a"));//输入指定字符的索引
        System.out.println(test.replace("J","w"));//替换指定字符
        System.out.println(test.length());//字符串长度
        System.out.println(test.trim());//去除字符串的空白
        System.out.println(test.getBytes(StandardCharsets.UTF_8));//???
        System.out.println(test.toLowerCase(Locale.ROOT));
        System.out.println(test.toUpperCase(Locale.ROOT));
        System.out.println(test.substring(5));
        System.out.println(test.equals(3));
        System.out.println(test1[7]);
        System.out.println(test.compareTo(abc));
    }
}
