package other;

import java.util.Arrays;
import java.util.Scanner;

public class delarray {
    //给定一个数组，删除第三个元素
    public static void main(String[] args){
        int array[]=new int[7];
        for (int i=0;i<=6;i++){
            Scanner sc=new Scanner(System.in);
            System.out.println("请输入第"+(i+1)+"个元素");
            int num= sc.nextInt();
            array[i]= num;
        }
        System.out.println("增加数字前的数组"+Arrays.toString(array));
        //找到删除数组的索引
        int index=0;
        for (int i=0;i<array.length;i++){
            if(array[i]==25){
                index=i;
                break;
            }
        }
        //删除第三个元素
        for (int i=index;i<= array.length-2;i++){
            array[i]=array[i+1];
        }
        array[array.length-1]=0;
        System.out.println("请输出变化后的数组"+Arrays.toString(array));


    }
}
