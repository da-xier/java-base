package other;

import java.util.Arrays;

public class Arrayscase {
    //写出定义数组的方法
    public static void main(String[] args) {
        String [] test1 =new String[3];
        test1[0]="1";
        test1[1]="2";
        test1[2]="json";
        System.out.println(Arrays.toString(test1));
        String [] test2={"3","4","uhy"};
        System.out.println(Arrays.toString(test2));
        String []test3 =new String[] {"5","6","deff"};
        System.out.println(Arrays.toString(test3));

    }
}
