package other;

import java.util.Scanner;

public class shoppingpoints {
    public static void main(String[] args) {
//        //1.给出积分
//        Scanner sc =new Scanner(System.in);
//        System.out.println("请输入会员的积分");
//        int score = sc.nextInt();
//        //2.根据积分判断折扣
//        if(score>=8000){
//            System.out.println("该会员享受的折扣是：0.6");
//        }else if (score>=4000){
//            System.out.println("该会员享受的折扣是：0.7");
//        }else if(score>=2000){
//            System.out.println("该会员享受的折扣是：0.8");
//        }else if(score<2000){
//            System.out.println("该会员享受的折扣是：0.9");
//        }
        //程序有问题，如果数是负数或者小数或者
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入会员的积分");
        if (sc.hasNextInt() == true) {
            int score = sc.nextInt();
            if(score>=0){
                String discount = "";
                if (score >= 8000) {
                    discount ="0.6";
                } else if (score >= 4000) {
                    discount ="0.7";
                } else if (score >= 2000) {
                    discount ="0.8";
                } else if (score < 2000) {
                    discount ="0.9";
                }
                System.out.println("该会员享受的折扣是"+discount);
            }else {
                System.out.println("积分不能为为负数");
            }

        } else {
            System.out.println("你录入的积分不是整数");
        }


    }
}


