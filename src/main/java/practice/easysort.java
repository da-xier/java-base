package practice;

/**
 * @ClassName easysort
 * @Author lupeibei
 * @create 2022/6/8 12:07 AM
 */
//将一组数据进行排序：arr={3,4,2,4,0,3};
public class easysort {
    public void rightcase(int [] arr){
        if (arr==null||arr.length<2){
            return;
        }
        int n=arr.length;
        for (int i = n-1; i >=0 ; i--) {
            for (int j = 1; j <= i; j++) {
                if (arr[j - 1] > arr[j]) {;
                    swp(arr, j-1, j);
                }
            }
        }
    }
    public void swp(int [] arr,int i,int j){
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }
    public void print(int [] arr){
        for (int i = 0; i < arr.length; i++) {
            if (i==0){
                System.out.print("[");
            }if(i==arr.length-1){
                System.out.println(arr[i]+"]");
            }else {
                System.out.print(arr[i]+ ",");
            }
        }
//        System.out.println();
    }
    public static void main(String[] args) {
        int [] arr={3,4,2,4,0,3};
        easysort easysort=new easysort();
        easysort.rightcase(arr);
        easysort.print(arr);

    }
}
