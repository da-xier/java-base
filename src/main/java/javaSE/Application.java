package javaSE;

/**
 * @ClassName Application
 * @Author lupeibei
 * @create 2022/5/29 11:12 PM
 */
//一个项目应该只存在一个main方法
public class Application {
    public static void main(String[] args) {
        //类是抽象的，需要实例化
        //类实例化会返回一个自己的对象
        //student对象就是一个student类的具体实例
        student xiaoming=new student();
        student xh=new student();
        xiaoming.name="xiaoming";
        xiaoming.age =3;
        System.out.println(xiaoming.name);
        System.out.println(xiaoming.age);

        System.out.println(xh.name);
        System.out.println(xh.age);








    }
}
