package javaSE;

/**
 * @ClassName student
 * @Author lupeibei
 * @create 2022/5/29 11:11 PM
 */
//学生类
public class student {
    //属性：字段
    String name;
    int age;
    //方法
    //this代表当前的类
    public  void study(){
        System.out.println(this.name+"在学习");
    }

}
