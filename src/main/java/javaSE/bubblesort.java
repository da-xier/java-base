package javaSE;

/**
 * @ClassName bubblesort
 * @Author lupeibei
 * @create 2022/5/29 7:00 PM
 */
public class bubblesort {
    public  static int[] bubble(int []arr) {
        int tmp=0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr.length-i-i ; j++) {
                if(arr[j+1]>arr[j]){
                    tmp=arr[j+1];
                    arr[j+1]=arr[j];
                    arr[j]=tmp;
                }
            }
        }
        return arr;
    }
    public static void main(String[] args) {
        int [] arr={3,4,2,5,-3,0};
        bubblesort bubblesort =new bubblesort();
        bubblesort.bubble(arr);
    }
}
