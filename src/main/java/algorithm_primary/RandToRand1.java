package algorithm_primary;

/**
 * @ClassName RandToRand1
 * @Author lupeibei
 * @create 2022/6/3 4:27 PM
 */
public class RandToRand1 {
    public static void main(String[] args) {
//        System.out.println("测试开始");
        int testTimes =1000000;
        int count=0;
//        for (int i = 0; i < testTimes; i++) {
//            if (Math.random()<0.75){
//                count++;
//            }
//        }
//        System.out.println((double) testTimes/(double) count);
        System.out.println("==========");
        count=0;
        //[0,1)-->[0,8)
//        double ans=Math.random()*8;
        for (int i = 0; i < testTimes; i++) {
            if (Math.random()*8<5){
                count++;
            }
        }
        //得到概率
        //验证[0,1)*一个数，得到的结果也是等概率的
            System.out.println((double) count/(double) testTimes);
        System.out.println((double) 5/(double) 8);

        //验证radom中[0,K)中取整数为[0,K-1]
        int k =9;
//        int ans =(int) (Math.random()*k);
        int [] counts=new int[9];
        for (int i = 0; i < testTimes; i++) {
            int ans =(int)(Math.random()*k);
            counts[ans]++;
        }
        for (int i = 0; i < k; i++) {
            System.out.println("这个数，出现了"+counts[i]+"次");
        }



    }
}
