package algorithm_primary;

/**
 * @ClassName RandToRand
 * @Author lupeibei
 * @create 2022/6/3 10:13 AM
 */
public class RandToRand {
    
    public static void main(String[] args) {
        System.out.println("测试开始");
        int testTimes =1000000;
        int count=0;
        for (int i = 0; i < testTimes; i++) {
            if (Math.random()<0.75){
                count++;
            }
        }
        System.out.println(testTimes);
        System.out.println(count);
        System.out.println(testTimes/count);
        //如果没有double类型，小数点后面的数会全部被省略无法取到小数点后面的数
        System.out.println((double)testTimes);
        System.out.println((double)count);
        System.out.println((double) testTimes/(double) count);
        ///(double) testTimes   (double) count
        
    }
}
