package sort;

/**
 * @ClassName insertsort
 * @Author lupeibei
 * @create 2022/5/25 10:08 AM
 */
public class insertsort {
    //写一个插入排序
    public static void insertsort(int [] arr) {
        if(arr==null||arr.length<2){
            return;
        }
        int n=arr.length;
        for (int end = 1; end  < n; end ++) {
            int newNumIndex =end;
            if (newNumIndex-1>=0 && arr[newNumIndex-1]>arr[newNumIndex]){
                swp(arr,newNumIndex-1,newNumIndex);
                newNumIndex--;
            }
        }
    }

    public static void swp(int [] arr,int i,int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }

    public static void printarrry(int [] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int [] arr={2,-34,-9,-23,99,321,345,9,236,2,1,66};
        printarrry(arr);
        insertsort(arr);
        printarrry(arr);


    }
}
