package sort;

/**
 * @ClassName pracselecsort
 * @Author lupeibei
 * @create 2022/5/25 8:15 AM
 */
//冒泡
public class pracselecsort {
    public static void bubblesort(int[] arr) {
        if(arr==null||arr.length<2){
            return;
        }
        //0~n-1
        //0~n-2
        //0~n-3  范围
        //0-end
        int n=arr.length;
        for (int end = n-1; end >=0; end--) {
            //0 1    1 2    2 3  3 4  4 5   end-1 end
            //定义second永远是第二个数，如果第二个数小于最后一个数就一直比对下一个数
            for (int second = 1; second <=end; second++) {
                if(arr[second-1]>arr[second]){
                    swp(arr,second-1,second);
                }
            }
        }
    }
    public static void swp(int[] arr ,int i,int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }
    public static void PraArray(int[] arr) {
        for (int i = 0; i <arr.length ; i++) {
            System.out.print(arr[i]+ " ");
        }
        System.out.println();

    }
    public static void main(String[] args) {
        int [] arr ={3,5,13,1345,-8,24,33,1,45,0,24,9};
        PraArray(arr);
        bubblesort(arr);
        PraArray(arr);

    }
}
