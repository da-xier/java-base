package sort;

/**
 * @ClassName praallsort
 * @Author lupeibei
 * @create 2022/5/26 8:21 AM
 */
public class praallsort {
    //写一下冒泡排序 将[-3,-4,-34,1,0,23,-45]进行排序
    public static void bobblesort(int [] arr){
        if(arr ==null||arr.length<2){
            return;
        }
        int N=arr.length;
        for (int i = N-1; i >=0 ; i--) {
            for (int second = 1; second < N; second++) {
                if(arr[second-1]>arr[second]){
                    swp(arr,second-1,second);
                }
            }
        }
    }

    public static void swp(int [] arr,int i,int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }

    public static void printarray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();

    }
    public static void main(String[] args) {
        int [] arr ={-3,-4,-34,1,0,23,-45};
        printarray(arr);
        bobblesort(arr);
        printarray(arr);

    }

}
