package sort;

import java.util.Arrays;

public class bubbleSort {
    //定义数组
    public static void main(String[] args) {
        int[] arr = {1,34,89,-54,38,-988,34};
        bubble(arr);
        System.out.println(Arrays.toString(arr));
    }

        public static void bubble(int[] arr){
            int temp = 0;
            if (arr.length <= 1) {
                System.out.println("数组长度小于等于1，不需要排序,输出的数组保持不变");
                return;
            }
            for (int i = 0; i < arr.length - 1; i++) {
                for (int j = 0; j < arr.length - 1 - i; j++) {
                    if (arr[j] > arr[j + 1]) {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
//            return temp;
        }
}

