package sort;

/**
 * @ClassName newinsertsort
 * @Author lupeibei
 * @create 2022/5/25 10:15 PM
 */
public class newinsertsort {
    public static void insertsort(int [] arr) {
        if(arr==null||arr.length<2){
            return;
        }
        int n=arr.length;
        for (int end = 1; end  < n; end ++) {
            //pre新数的前一个位置   end-1 end
            for (int pre = end-1; pre >=0&& arr[pre]>arr[pre+1] ; pre--) {
                swp(arr,pre,pre+1);
                
            }
            
        }
    }

    public static void swp(int [] arr,int i,int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }

    public static void printarrry(int [] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int [] arr={2,-34,-9,-23,99,321,345,9,236,2,1,66};
        printarrry(arr);
        insertsort(arr);
        printarrry(arr);


    }
}
