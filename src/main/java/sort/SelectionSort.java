package sort;

/**
 * @ClassName SelectionSort
 * @Author lupeibei
 * @create 2022/5/24 9:19 AM
 */
public class SelectionSort {
    //写一些冒泡排序
    public static void bubblesort(int [] arr) {
        if(arr==null||arr.length<2){
            return;
        }
        int N=arr.length;
        for (int end = N-1; end >=0 ; end--) {
            for (int second = 1; second <=end ; second++) {
                if (arr[second-1]>arr[second]){
                    swp(arr,second-1,second);
                }
            }
        }
    }
    //写插入排序
    public static void selectsort(int [] arr) {
        if(arr==null||arr.length<2){
            return;
        }
        int n=arr.length;
        for (int i = 0; i < n; i++) {
            int minValueindex =i;
            for (int j = i+1; j < n; j++) {
                if (arr[j]<arr[minValueindex]){
                    swp(arr,minValueindex,j);
                }
            }

        }
    }
    //写插入排序(新插入)
    public static void insertsort(int[] arr) {
        if(arr==null||arr.length<2){
            return;
        }
        int N=arr.length;
        for (int end = 1; end <N ; end++) {
            int newNumIndex=end;
            if (newNumIndex-1>=0 && arr[newNumIndex-1]>arr[newNumIndex]){
                swp(arr,newNumIndex-1,newNumIndex);
                newNumIndex--;
            }
            }
        }



    public static void swp(int [] arr,int i ,int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }
    public static void printarray(int [] arr) {
        for (int i = 0; i <arr.length ; i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int [] arr={-7,-344,-2,-4,13,4-5,0,2,1,1123,445,23,10,-355};
        printarray(arr);
        insertsort(arr);
        printarray(arr);
    }
}

