package sort;

/**
 * @ClassName newbubblesort
 * @Author lupeibei
 * @create 2022/5/29 5:53 PM
 */
public class newbubblesort {
    //[1, 7, 8, 4, 3]  最后数组展示为 [1, 3, 4, 7, 8]
    public  void bubblesort(int [] arr) {
        if (arr ==null ||arr.length<2){
            return;
        }
        int N=arr.length;
        for (int end = N-1; end >=0; end--) {
            for (int second = 1; second <=end; second++) {
                if (arr[second-1]>arr[second]){
                    swp(arr,second-1,second);
                }

            }
        }
    }
    public  void swp(int [] arr,int i,int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }

    public  void printarray(int [] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i==0){
                System.out.print("[");
            }if(i==arr.length-1){
                System.out.println(arr[i]+"]");
            }else {
                System.out.print(arr[i]+ ",");
            }
        }
//        System.out.println();
    }
    public static void main(String[] args) {
        int [] arr ={1, 7, 8, 4, 3};
        //为了不那么调用静态方法，
        newbubblesort newbubblesort =new newbubblesort();
        newbubblesort.bubblesort(arr);
        newbubblesort.printarray(arr);
//        printarray(arr);
//        bubblesort(arr);
//        printarray(arr);




    }

}
