package algorithm;

/**
 * @ClassName position32
 * @Author lupeibei
 * @create 2022/5/24 8:33 AM
 */
public class position32 {
    //打印任意整数的32位
    public static void main(String[] args) {
        int num =4;
        print(num);
    }

    public static void print(int num) {
        for (int i = 31; i >=0 ; i--) {
            System.out.print((num&(1<<i))==0?"0":"1");
        }
        System.out.println();

    }
}
