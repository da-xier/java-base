package com.hog;

import org.junit.jupiter.api.Test;

/**
 * @ClassName FirstJunitTest
 * @Author lupeibei
 * @create 2022/6/13 11:25 PM
 */
public class FirstJunitTest {
    @Test
    void first(){
        System.out.println("第一个Junit5测试用例");
    }
    @Test
    void sec() {
        System.out.println("第二个Junit5测试用例");
    }
}
