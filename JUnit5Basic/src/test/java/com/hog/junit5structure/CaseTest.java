package com.hog.junit5structure;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @ClassName CaseTest
 * @Author lupeibei
 * @create 2022/6/23 9:40 PM
 */
public class CaseTest {
    @BeforeEach
    void setUp(){
        System.out.println("testcase每条用例执行的前置动作");
    }
    @AfterEach
    void tearDown(){
        System.out.println("testcase每条用例执行的后置动作");
    }

    @Test
    @DisplayName("第一条")
    //修改测试用例的名称
    void hog(){
        System.out.println("第一条测试用例");
        //断言,第一个结果是预期结果，第二个结果是实际结果（运行的步骤也需要写上）
        assertEquals(2,1+1);
    }
    @Test
    @DisplayName("第二条")
        //修改测试用例的名称
    void hog2(){
        System.out.println("第二条测试用例");
        //断言,第一个结果是预期结果，第二个结果是实际结果（运行的步骤也需要写上）
        assertEquals(2,1+1);
    }

    }

